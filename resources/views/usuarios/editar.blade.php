@include('Modulacion.header')
<body class="bg-gradient-primary">
 
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        @if(Auth::user()->rol_id ==1 ||Auth::user()->rol_id ==2)
        <div class="row">
          
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Editar Usuario</h1>
              </div>
            <form class="user"  action="{{URL('/User'.'/'.$users->id)}}" method="post" enctype="multipart/form-data">
             {{ csrf_field()}}
             {{method_field('PATCH')}}
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                <input type="text" class="form-control form-control-user" id="name" name="name" placeholder="Nombre" value="{{$users->name}}">
                </div>
                <div class="col-sm-6">
                  <input type="text" class="form-control form-control-user" id="Cargo" name="rol_id"placeholder="Cargo"  value="{{$users->rol_id}}" >
                </div>
              </div>
              <div class="form-group">
                <input type="email" class="form-control form-control-user" id="Email" name="email" placeholder="Correo Electronico"  value="{{$users->email}}">
              </div>
              <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                  <input type="password" class="form-control form-control-user" id="contrasena"  name="password" placeholder="Password" >
                </div>
                <div class="col-sm-6">
                  <input type="hidden" class="form-control form-control-user" id="Ccontrasena" placeholder="Repeat Password">
                </div>
              </div>
                <input type="submit" class= "btn btn-primary btn-user btn-block" value="Editar cuenta de usuario">
               
              </form>
              @else
              <div class="col-lg-7">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Aun no dispone de este recurso</h1>
                  </div>
              </div>
            </div>
          </div>
          @endif
        </div>
         </div>
              <!-- /.container-fluid -->
      
            </div>
                @include('Modulacion.footer')