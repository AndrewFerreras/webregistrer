@include('Modulacion.header')



<div class="row">

  <!-- Area Chart -->
  <div class="col-xl-8 col-lg-7">
    <div class="card shadow mb-4">
      <!-- Card Header - Dropdown -->
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Lista de Usuarios</h6>
        <div class="dropdown no-arrow">
         
          
        </div>
      </div>
      <!-- Card Body -->
     
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
           
             <th>ID</th>
             <th>Nombre</th>
             <th>Correo Electronico</th>
             <th>Editar</th>
             <th>Eliminar</th>
         </thead>
         <tbody>
          @foreach ($user as $users)
          <tr>
              <td>{{ $users->id }}</td>
              <td>{{ $users->name }}</td>
              <td>{{ $users->email}} </td>

          <td> <a class="btn btn-google btn-user btn-block" href="{{URL('/User'.'/'.$users->id.'/edit')}}">Editar</a></td>
          <td> <a class="btn btn-google btn-user btn-block" href="{{URL('/User'.'/'. $users->id.'/delete')}}">Eliminar</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>

        


        </div>
      </div>
    </div>
  </div>
   </div>
        <!-- /.container-fluid -->

      </div>
  @include('Modulacion.footer')