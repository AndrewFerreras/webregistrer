<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/administrador/admin',function()
{
    return view('administrador.admin');
})->name('admin');


//usuarios
Route:: get('/Usuarios','UserController@index')->name("ListaUsuarios");
Route::get('/usuarios/registrar','UserController@registrar')->name('registrarUsuario');
Route::post('/User','UserController@store')->name('User.store');
Route::get('/User/{User}/edit','UserController@edit');
Route::patch('/User/{User}','UserController@update');
Route::get('/User/{User}/delete','UserController@destroy');
