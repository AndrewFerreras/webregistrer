<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=User::all();

        return view ("usuarios.Usuarios",compact('user'));
    }
    public function Registrar()
    {
       

        return view ("usuarios.Crear");
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos=request()->except('_token','Ccontrasena');
        $datos["password"]=encrypt($datos["password"]);
        
        User::insert($datos);
        return view ("usuarios.Crear");
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users=User::findOrfail($id);
        return view('usuarios.editar',compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update( $id)
    {
        $datos=request()->except('_token','Ccontrasena','_method');
        User::where('id','=',$id)->update( $datos);
        $users=User::findOrfail($id);
        return view('usuarios.editar',compact('users'));

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
    
        User::destroy($id);
        return redirect('administrador.admin');
    }
}
