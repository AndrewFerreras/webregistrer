<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([    
            'id'=>1,
            'name'=>'Juan Perez',
            'email'=>'fda@hotmail.com',
            'email_verified_at' => now(),
            'rol_id'=>1,
            'password'=>bcrypt('A13121998'),
            'remember_token' => Str::random(10)
        
        ]);

        $users=factory(App\User::class,10)->create();
    }
}
