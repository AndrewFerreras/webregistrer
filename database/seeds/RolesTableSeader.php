<?php

use Illuminate\Database\Seeder;

class RolesTableSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([    
            'id'=>1,
            'name'=>'administrador',
            'description'=>'Personar sin restricciones',
            
        
        ]);
        DB::table('roles')->insert([    
            'id'=>2,
            'name'=>'Supervisor',
            'description'=>'Personar limitado a su area y solo lectura en algunos casos',
            
        
        ]);
        
        DB::table('roles')->insert([    
            'id'=>3,
            'name'=>'Visitante',
            'description'=>'Solo lectura',
            
        
        ]);

    }
}
